
<!---
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel indicators -
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
        <!-- Wrapper for carousel items
        <div class="carousel-inner">
            <div class="item active">
                <img src="wp-content/uploads/2016/03/slider_img_3.jpg" alt="First Slide">
            </div>
            <div class="item">
                <img src="wp-content/uploads/2016/03/slider_img_2.jpg" alt="Second Slide">
            </div>
            <div class="item">
                <img src="wp-content/uploads/2016/03/slider_img_1.jpg" alt="Third Slide">
            </div>
        </div>
        <!-- Carousel controls 
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
-->

</head>
<body>
    <!-- Wrapper for slides -->
    <div class="container col-md-9">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img src="wp-content/uploads/2016/03/slider_img_1.jpg" alt="Chania" width="460" height="345">
        <div class="carousel-caption">
        <button type="button" class="btn btn-info btn-sm buttonlocation">Readmore</button>
          <h3>WAH Scale UP</h3>
          <p class="textslider textblock">Add text</p>
          <p class="btn btn-info btn-sm"></p>
        </div>
      </div>

      <div class="item">
        <img src="wp-content/uploads/2016/03/slider_img_2.jpg" alt="Chania" width="460" height="345">
        <div class="carousel-caption">
        <button type="button" class="btn btn-info btn-sm buttonlocation">Readmore</button>
          <h3>WAH4H</h3>
         <p class="textslider textblock">Add text</p>
        </div>
      </div>
    
      <div class="item">
        <img src="wp-content/uploads/2016/03/slider_img_3.jpg" alt="Flower" width="460" height="345">
        <div class="carousel-caption">
        <button type="button" class="btn btn-info btn-sm buttonlocation">Readmore</button>
          <h3>4 Provinces</h3>
          <p class="textslider textblock">Add text</p>
        </div>
      </div>

      <div class="item">
        <img src="wp-content/uploads/2016/03/slider_img_1.jpg" alt="Flower" width="460" height="345">
        <div class="carousel-caption">
        <button type="button" class="btn btn-info btn-sm buttonlocation">Readmore</button>
          <h3>Scale Up</h3>
        <p class="textslider textblock">Add text</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
        
            <div class="col-md-8">
            </div>
        
     
