<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wahsitewordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8Y87o33yw&FL=OQ@&zHsN<=fv lPj!]9}D=c>0b+()I1B!sAnsIz@*BbS(;IBlh0');
define('SECURE_AUTH_KEY',  'pY:}%cQQQFY1-?qc.)4KpNneFZv!5SwJm-L~+OF8=D!E jizo7,I[!~s@b$HtXlx');
define('LOGGED_IN_KEY',    '*gHRmRtP#HWz@,r)n5g2Lw1>O<i`7zdAGv_@}:Um -Y;NV/4Mrfh&SIpPgRGRWAz');
define('NONCE_KEY',        'J}h:2k.0q?7cW%!~!UXDTPP?^N&3b{?zB;7:9hn+[NIQ86|5B$#I7#wemBCJv*FC');
define('AUTH_SALT',        '+iCA|GA!1`i1LKv@au.p;[[+bh<0 BRV56k.LnJXynn|;_ 5eoo6f[|&k5xqT<h?');
define('SECURE_AUTH_SALT', 'C*G|CC?)xVDBc+2|{wG56f0|sJh8:&D/V,q9P$v^0p-dlO2T;<7@bWmY@anQ8;OQ');
define('LOGGED_IN_SALT',   ')hkpWRoy,g=85^qx/c/{gDbz`[MP5AQ_B,Bjy+qGyLK4&CX,+Yw@w,xpyI38FNUl');
define('NONCE_SALT',       'w]E+%R3-nEn3PYiql7!q:^SO-bhBe4^S Vw|9[=E:>j0zA<7^y7RH9SH+Or+8tJ~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
